<?php
        session_start();
        ?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="js/javascript.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <title>Formulaire</title>
    </head>




<body>
    <h1>Projet Web</h1>
<div class="form">
    
    <form action="reponse.php" method="post" id="survey" name ="survey" onsubmit="return validateForm()">
    <p>Quel est votre prénom ? </p>
    <input id = "prenom" name="prenom" type="text" required>
    <span style="color:red" id='wrongName'></span><br>
    <p>Quel est votre âge ? </p>
    <input id="age" type="text" name="age" required>
    <span style="color:red" id='wrongAge'></span><br>
    <input type="submit" value="valider" name="valider" class ="button" id="valider" onclick="">

    </form> 
        </div>
</body>
</html>


<!-- -	un formulaire HTML bien fait qui demande le prénom et l’âge d’une personne
-	ce formulaire comprendra un JS qui vérifiera que la saisie est bonne (regex) pour le prénom (que des lettres des espaces et 1 tiret entre 2 mots) ainsi que l’âge (que des chiffres compris entre 18 et 80)
-	une fois transmis ce formulaire en post, on réceptionne les variables dans une session (toujours en vérifiant la crédibilité des valeurs reçues)
-	on les affiche via la session dans un simple page web qui dit « Bonjour prénom, tu as xx ans. » -->
