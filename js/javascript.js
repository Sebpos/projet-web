  function validateForm() {

      // Get form via form name:
      var myForm = document.forms["survey"];
      var prenom = myForm["prenom"].value;
      var age = myForm["age"].value;
      var prenomValid = new RegExp(/^[A-Z][a-zéïîö]+([-'\s][a-zA-Z][a-z]+)?$/);

      if (prenomValid.test(prenom) == false) {
          myForm["prenom"].focus();
          document.getElementById("wrongName").textContent = "Format incorrect";
          return false;


      } else {
          document.getElementById("wrongName").textContent = "";

      }

      if (isNaN(age)) {
          document.getElementById("wrongAge").textContent = "Format d'âge incorrect";
          return false;
      } else {
          document.getElementById("wrongAge").textContent = "";
          if (age < 18 || age > 80) {
              document.getElementById("wrongAge").textContent = "Âge incorrect";
              return false;
          } else {
              document.getElementById("wrongAge").textContent = "";
          }

      }
      return true;
  }